//
//  Pull.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 10/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import Foundation

class Pull: Serializable {
    var title:String?
    var body:String?
    var user:Owner?
}
