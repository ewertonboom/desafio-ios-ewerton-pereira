//
//  SwiftHelpers.swift
//
//  Created by Ewerton Ariel Apolonio Pereira on 21/07/15.
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

public struct ModuleHelper {
    

    public static func hasInvalidEmail(email:String) -> Bool {
        var valid = false
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: [.CaseInsensitive])
            let match = regex.firstMatchInString(email, options: NSMatchingOptions.ReportProgress, range: NSMakeRange(0, email.characters.count))
            if ((match?.numberOfRanges) == nil) {
                valid = true
            }
        }catch {
            
        }
        return valid
    }
    
    public static func carregarPropriedades(classe:AnyClass, obj:AnyObject, target:AnyObject, fieldType:Int?){
        var count : UInt32 = 0
        // let classToInspect:AnyClass = classe
        let properties : UnsafeMutablePointer <objc_property_t> = class_copyPropertyList(classe, &count)
        var propertyNames : [String] = []
        let intCount = Int(count)
        for i in 0 ..< intCount {
            let property : objc_property_t = properties[i]
            let propertyName:String = NSString(UTF8String: property_getName(property))! as String
            propertyNames.append(propertyName as String)
            
            var propertyNameOnly = propertyName.stringByReplacingOccurrencesOfString("txt", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            propertyNameOnly = propertyNameOnly.stringByReplacingOccurrencesOfString("text", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            propertyNameOnly = propertyNameOnly.stringByReplacingOccurrencesOfString("segment", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            propertyNameOnly = propertyNameOnly.stringByReplacingOccurrencesOfString("lbl", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            let objValue:AnyObject = self.getObjPropertyValue(obj, property: propertyNameOnly)
            
            if propertyName.containsString("txt") {
                
                let field:UITextField = target.valueForKey(propertyName as String) as! UITextField
                
                if objValue.isKindOfClass(NSString) {
                    field.text = objValue as? String
                }
                
                if objValue.isKindOfClass(NSNumber) {
                    let nvalue = objValue as! NSNumber
                    field.text = nvalue.stringValue
                }
                
                if fieldType != nil {
                    if field.tag == fieldType {
                        field.text = field.text!.serverDateToBrazilianDate
                    }
                    
                    if field.tag == fieldType {
                        field.text = field.text!.cpfMasked
                    }
                }
            }
            
            if propertyName.containsString("lbl") {
                
                let field:UILabel = target.valueForKey(propertyName as String) as! UILabel
                
                if objValue.isKindOfClass(NSString) {
                    field.text = objValue as? String
                }
                
                if objValue.isKindOfClass(NSNumber) {
                    let nvalue = objValue as! NSNumber
                    field.text = nvalue.stringValue
                }
                
                if fieldType != nil {
                    if field.tag == fieldType {
                        field.text = field.text!.serverDateToBrazilianDate
                    }
                    
                    if field.tag == fieldType {
                        field.text = field.text!.cpfMasked
                    }
                }
            }
            
            if propertyName.containsString("text"){
                let field:UITextView = target.valueForKey(propertyName as String) as! UITextView
                field.text = objValue as! String
            }
            
            if propertyName.containsString("segment"){
                let field:UISegmentedControl = target.valueForKey(propertyName as String) as! UISegmentedControl
                if let _ = objValue as? Int{
                    field.selectedSegmentIndex = objValue as! Int
                }
            }
            
        }
        
        free(properties)
        //println(propertyNames)
    }
    
    private static func getObjPropertyValue(obj:AnyObject, property:String)->AnyObject{
       // let entiti = obj.properties
        
        if obj.respondsToSelector(NSSelectorFromString(property.lowercaseFirstLetter)){
        if let retorno = obj.valueForKey(property.lowercaseFirstLetter) {
            return retorno
        }
        }
        return ""
    }

}

//MARK: - EXTENSION NSDICTIONARY
extension NSDictionary {
    var removeNullValues:NSDictionary{
        let mutableDict:NSMutableDictionary = NSMutableDictionary(dictionary: self)
        removeNullFromDict(mutableDict)
        return mutableDict
    }
    
    func mergeDictWithKey(key:String) -> NSDictionary{
        let tempDict = NSMutableDictionary(dictionary: self)
        
        if self.objectForKey(key)!.isKindOfClass(NSDictionary){
            let keyDict = self.objectForKey(key) as! NSDictionary
            tempDict.removeObjectForKey(key)
        
            for (k, value) in keyDict {
                if !value.isKindOfClass(NSNull) && k as! String != "id" {
                    tempDict.setObject(value, forKey: k as! String)
                }
            }
        }
        
        return tempDict
    }

    
    func removeNullFromDict(dict:NSMutableDictionary) -> NSDictionary{
        for  (key, _) in dict {
            if dict.objectForKey(key)!.isKindOfClass(NSNull){
                dict.removeObjectForKey(key)
            }else if dict.objectForKey(key)!.isKindOfClass(NSDictionary) {
                dict.setObject(removeNullFromDict(NSMutableDictionary(dictionary:dict.objectForKey(key) as! NSDictionary)), forKey: key as! String)
            }
        }
        return dict
    }
}


//MARK: - EXTENSION STRING
extension String {
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    var camelCasedString: String {
        let result = NSMutableString()
        var lastWasUnderscore = false
        
        for character in self.characters {
            var newCharacter = String(character)
            if lastWasUnderscore  {
                newCharacter = newCharacter.capitalizedString
            }
            lastWasUnderscore = newCharacter == "_"
            if lastWasUnderscore {
                newCharacter = ""
            }
            result.appendString(newCharacter)
        }
        return result as String
    }
    
    var capitalizeFirstLetter:String{
        
        let first = self.startIndex
        
        let rest = first.advancedBy(1)..<self.endIndex
        
        return self[first...first].uppercaseString + self[rest]
    }
    
    var lowercaseFirstLetter:String{
        
        let first = self.startIndex
        
        let rest = first.advancedBy(1)..<self.endIndex
        
        return self[first...first].lowercaseString + self[rest]
    }
    
    var delegaciaIdPattern:String{
        let formater = NSDateFormatter()
        formater.dateFormat = "dd/MM/yyyy"
        var data = formater.dateFromString(self)
        
        if data == nil {
            formater.dateFormat = "MM/yyyy"
            data = formater.dateFromString(self)
        }
        
        if data == nil {
            return ""
        }
        formater.dateFormat = "yyyyMMddHHmmSSS"
        return formater.stringFromDate(data!)
        
    }
    
    var serverDateRFCString:String{
        let formater = NSDateFormatter()
        formater.dateFormat = "dd/MM/yyyy"
        var data = formater.dateFromString(self)
        
        if data == nil {
            formater.dateFormat = "MM/yyyy"
            data = formater.dateFromString(self)
        }
        
        if data == nil {
            return ""
        }
        formater.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        return formater.stringFromDate(data!)
    }
    
    var dateFromFormat:String{
        let formater = NSDateFormatter()
        
        formater.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        let data = formater.dateFromString(self)
        if data == nil {
            return ""
        }
        
        formater.dateFormat = "dd/MM/yyyy"
        return formater.stringFromDate(data!)
    }
    
    var hourFromFormat:String{
        let formater = NSDateFormatter()
        
        formater.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        let data = formater.dateFromString(self)
        if data == nil {
            return ""
        }
        
        formater.dateFormat = "HH:mm"
        return formater.stringFromDate(data!)
    }
    
    var serverDateRFCWithHourString:String{
        let formater = NSDateFormatter()
        formater.dateFormat = "dd/MM/yyyy HH:mm"
        let data = formater.dateFromString(self)
    
        if data == nil {
            return ""
        }
        formater.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        return formater.stringFromDate(data!)
    }
    
    var serverDateString:String{
        let formater = NSDateFormatter()
        formater.dateFormat = "dd/MM/yyyy"
        var data = formater.dateFromString(self)
        
        if data == nil {
            formater.dateFormat = "MM/yyyy"
            data = formater.dateFromString(self)
        }

        if data == nil {
            return ""
        }
        
        formater.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return formater.stringFromDate(data!)
    }
    
    var serverDateToBrazilianDate:String{
        let formater = NSDateFormatter()
        formater.dateFormat = "yyyy-MM-dd hh:mm:ss"
        var data = formater.dateFromString(self)
        
        if data == nil {
            formater.dateFormat = "yyyy-MM-dd"
            data = formater.dateFromString(self)
            if data == nil {
                return ""
            }
        }
        
        formater.dateFormat = "dd/MM/yyyy"
        
        return formater.stringFromDate(data!)
    }
    
    var currencyMask:String {
        
        let currency = NSMutableString(string:self)
        let formatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "pt-BR")
        let numberFromField = (NSString(string: currency).doubleValue)/100
        
        return formatter.stringFromNumber(numberFromField)!
    }

    
    var cnpjMasked:String{
        let cnpj =  NSMutableString(string:self)
        if cnpj.length > 0{
            if cnpj.length < 18{
                if cnpj.length > 2{
                    cnpj.insertString(".", atIndex: 2)
                }
                if cnpj.length > 6{
                    cnpj.insertString(".", atIndex: 6)
                }
                if cnpj.length > 10{
                    cnpj.insertString("/", atIndex: 10)
                }
                if cnpj.length > 15{
                    cnpj.insertString("-", atIndex: 15)
                }
        }
        }
        
        return (cnpj as NSString) as String
    }
    
    var placaMasked:String{
        let placa =  NSMutableString(string:self.cleanMask)
        
        if placa.length > 0 {
            if placa.length < 18 {
                if placa.length > 3 {
                    placa.insertString("-", atIndex: 3)
                }
            }
        }
        
        return (placa as NSString) as String
    }
    
    var cpfMasked:String{
        let cpf =  NSMutableString(string:self)
        if cpf.length > 0{
            if cpf.length < 14
            {
                if cpf.length > 3{
                    cpf.insertString(".", atIndex: 3)
                }
                
                if cpf.length > 7{
                    cpf.insertString(".", atIndex: 7)
                }
                
                if cpf.length > 11{
                     cpf.insertString("-", atIndex: 11)
                }
            }
        }
        return (cpf as NSString) as String
    
    }
    
    var cleanMask:String{
        
        var stringLimpa = self.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString(".", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString(",", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString("/", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString("R$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString(")", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        stringLimpa = stringLimpa.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        return stringLimpa
    }
}

extension Int {
    
    func toBool () ->Bool? {
        
        switch self {
            
        case 0:
            
            return false
            
        case 1:
            
            return true
            
        default:
            
            return nil
            
        }
        
    }
    
}

extension NSFileManager {
    class func documentsDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as [String]
        return paths[0]
    }
    
    class func cachesDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true) as [String]
        return paths[0]
    }
}

//MARK: - EXTENSION UIIMAGE
extension UIImage
{
    func tint(color: UIColor, blendMode: CGBlendMode) -> UIImage
    {
        let drawRect = CGRectMake(0.0, 0.0, size.width, size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 0.0, size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        CGContextClipToMask(context, drawRect, CGImage)
        color.setFill()
        UIRectFill(drawRect)
        drawInRect(drawRect, blendMode: blendMode, alpha: 1.0)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return tintedImage
    }
}

//MARK: - EXTENSION NSOBJECT
import Foundation

enum PropertyTypes:String
{
    case OptionalInt = "Optional<Int>"
    case Int = "Int"
    case OptionalString = "Optional<String>"
    case String = "String"
    //...
}

extension NSObject{
    //returns the property type
    func getTypeOfProperty(name:String)->String?
    {
        let type: Mirror = Mirror(reflecting:self)
        for child in type.children {
            if child.label! == name
            {
                return String(child.value.dynamicType)
            }
        }
        return nil
    }
    
    //Property Type Comparison
    func propertyIsOfType(propertyName:String, type:PropertyTypes)->Bool
    {
        if getTypeOfProperty(propertyName) == type.rawValue
        {
            return true
        }
        
        return false
    }
}
