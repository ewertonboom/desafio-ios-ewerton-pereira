//
//  RepoTableViewController.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 07/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class RepoTableViewController: UITableViewController {
    let cellIdentifier = "cellIdentifier"
    var itens = [Repo]()
    var page = 1
    var repo:Repo?
    var isLoading = false
    var haveMore = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.registerNib(UINib(nibName: "RepoTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier: self.cellIdentifier)
        
        request()
        
    }
    
    func request () {
        isLoading = true
        Alamofire.request(.GET, "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(self.page)", parameters: ["t":"1"])
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    
                    let result = Itens(dados:JSON as! NSDictionary)
                    
                    if result.items != nil {
                        self.itens.appendContentsOf(result.items!)
                        self.tableView.reloadData()
                    }else{
                        self.haveMore = false
                    }
                }else{
                    self.haveMore = false
                }
                
                self.page = self.page + 1
                self.isLoading = false
        }

        
    }
    


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itens.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! RepoTableViewCell

        let repo = self.itens[indexPath.row]
        
        cell.lblRepoName.text = repo.name
        cell.lblDescription.text = repo.descriptions
        cell.lblForks.text = repo.forks?.stringValue
        cell.lblWatchers.text = repo.watchers?.stringValue
        
        cell.lblUsername.text = repo.owner?.login
        cell.lblUserFullName.text = repo.owner?.type
        
        if let avatarUrl = repo.owner?.avatarUrl {
            let url = NSURL(string: avatarUrl)            
            cell.imgAvatar.sd_setImageWithURL(url)
    
        }
        
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.repo = self.itens[indexPath.row]
        performSegueWithIdentifier("repoSegueIdentifier", sender: self)
    }
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let lastElement = self.itens.count - 1
        if indexPath.row == lastElement {
            if (!self.isLoading && self.haveMore) {
                self.request()
            }
        }
    }
    
 
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "repoSegueIdentifier" && repo != nil {
            let nextVc = segue.destinationViewController as! PullRequestTableViewController
            nextVc.repo = repo
            
        }
    }
    

}
