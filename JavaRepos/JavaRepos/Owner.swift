//
//  Owner.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 10/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import Foundation

class Owner: Serializable {
    var avatarUrl:String?
    var login:String?
    var type:String?
}
