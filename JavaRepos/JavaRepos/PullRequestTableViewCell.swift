//
//  PullRequestTableViewCell.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 10/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var lblPullName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgAvatar.layer.cornerRadius = 20
        self.imgAvatar.layer.masksToBounds = true
    }

  
    
}
