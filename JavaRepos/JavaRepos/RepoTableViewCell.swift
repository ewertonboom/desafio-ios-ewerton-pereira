//
//  RepoTableViewCell.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 08/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {
    @IBOutlet weak var lblRepoName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblUserFullName: UILabel!
    @IBOutlet weak var lblForks: UILabel!
    @IBOutlet weak var lblWatchers: UILabel!
    
    @IBOutlet weak var imgAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgAvatar.layer.cornerRadius = 25
        self.imgAvatar.layer.masksToBounds = true
    }

   
    
}
