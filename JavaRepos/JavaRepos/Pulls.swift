//
//  Pulls.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 10/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import Foundation

class Pulls: Serializable {
    
    var itens:[Pull]?
    
    required init(array: NSArray) {
        super.init(dados:["itens":array])
        
    }
    
    required init(dados: NSDictionary) {
        fatalError("init(dados:) has not been implemented")
    }
}
