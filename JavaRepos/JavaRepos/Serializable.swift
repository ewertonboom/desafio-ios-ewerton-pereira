//
//  Serializable.swift
//
//  Created by Ewerton Ariel Apolonio Pereira on 15/06/15.
//  Copyright (c) 2015. All rights reserved.
//

import Foundation

class Serializable: Deserializable {

    var mensagem:String?
    var flagErro:NSNumber?
    var codigoRetorno:NSNumber?
    var codigoMensagem:NSNumber?
    var dataServidor:String?
    
    override init() {
        super.init()
    }
    
    required init(dados:NSDictionary) {
        super.init()
        
        for (key, value) in dados {
            
            var keyName =  (key as! String)
            keyName = keyName.camelCasedString
            
            if !value.isKindOfClass(NSNull) && self.respondsToSelector(NSSelectorFromString(keyName)) {
                if value.isKindOfClass(NSDictionary) || value.isKindOfClass(NSArray) {
                    
                    if var type = self.getTypeOfProperty(keyName) {
                        type = type.stringByReplacingOccurrencesOfString("NSDictionary<", withString: "")
                        type = type.stringByReplacingOccurrencesOfString("Array<", withString: "")
                        type = type.stringByReplacingOccurrencesOfString("Optional<", withString: "")
                        type = type.stringByReplacingOccurrencesOfString(">", withString: "")
                        
                        if let arr = dados[keyName] as? NSArray {
                            var itensArr = Array<Serializable>()
                            var itensString = Array<String>()
                            for i in 0 ..< arr.count {
                                
                                let item = arr[i]
                                if (item.isKindOfClass(NSDictionary)){
                                    let dtoClass = Mapping[type] as? Serializable.Type
                                    let dto = dtoClass!.init(dados: item as! NSDictionary)
                                    itensArr.append(dto)
                                }else if item.isKindOfClass(NSString){
                                    itensString.append(item as! String)
                                }else{
                                    print("fix it !!")
                                }
                            }
                            if itensArr.count > 0 {
                                self.setValue(itensArr, forKey: keyName)
                            }
                            
                            if itensString.count > 0 {
                                self.setValue(itensString, forKey: keyName)
                            }
                            
                        }else if let dict = dados[keyName] as? NSDictionary {
                            let dtoClass = Mapping[type] as? Serializable.Type
                            let dto = dtoClass!.init(dados: dict)
                            self.setValue(dto, forKey: keyName)
                        }
                    }
                }else{
                    
                    if(self.respondsToSelector(NSSelectorFromString(keyName))) {
                        if keyName != "description" {
                            self.setValue(value, forKey: keyName)
                        }
                    }
                
                }
                
            }
        }

    }
    
}


