//
//  Mapping.swift
//
//  Created by Ewerton Ariel Apolonio Pereira on 20/04/16.
//  Copyright © 2016. All rights reserved.
//

import Foundation

let Mapping = [
    "Repo": Repo.self,
    "Owner": Owner.self,
    "Pull": Pull.self
]