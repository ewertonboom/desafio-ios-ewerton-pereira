//
//  PullRequestTableViewController.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 10/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import UIKit
import Alamofire
class PullRequestTableViewController: UITableViewController {
    let cellIdentifier = "cellIdentifier"
    var itens = [Pull]()
    var page = 1
    var repo:Repo!
    
    var isLoading = false
    var haveMore = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = repo.name
        self.tableView.registerNib(UINib(nibName: "PullRequestTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier: self.cellIdentifier)
        
        request()
        
    }
    
    func request () {
        let creator = repo.owner!.login!
        let reponame = repo.name!
        
        Alamofire.request(.GET, "https://api.github.com/repos/\(creator)/\(reponame)/pulls", parameters: ["t":"1"])
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    
                    let result = Pulls(array:JSON as! NSArray)
                    
                    if result.itens != nil {
                        self.itens = result.itens!
                        self.tableView.reloadData()
                    }else{
                        self.haveMore = false
                    }
                }else{
                    self.haveMore = false
                }
                
                self.page = self.page + 1
                
                self.isLoading = false
        }
        
        
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itens.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PullRequestTableViewCell
        
        let pull = self.itens[indexPath.row]
        
        cell.lblPullName.text = pull.title
        cell.lblDescription.text = pull.body
        
        cell.lblUsername.text = pull.user?.login
        cell.lblType.text = pull.user?.type
        
        if let avatarUrl = pull.user?.avatarUrl {
            let url = NSURL(string: avatarUrl)
            cell.imgAvatar.sd_setImageWithURL(url)
            
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let lastElement = self.itens.count - 1
        if indexPath.row == lastElement {
            if (!self.isLoading && self.haveMore) {
                self.request()
            }
        }
    }

}
