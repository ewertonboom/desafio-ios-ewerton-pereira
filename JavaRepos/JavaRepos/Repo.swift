//
//  Repo.swift
//  JavaRepos
//
//  Created by Ewerton Pereira on 08/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import Foundation

class Repo:Serializable {

    var name: String?
    var forks: NSNumber?
    var watchers: NSNumber?
    var descriptions:String?
    var owner:Owner?
    
    required init(dados: NSDictionary) {
        super.init(dados: dados)
        
        self.descriptions = dados.objectForKey("description") as? String
    }
  
    /*
    "archive_url" = "https://api.github.com/repos/ReactiveX/RxJava/{archive_format}{/ref}";
    "assignees_url" = "https://api.github.com/repos/ReactiveX/RxJava/assignees{/user}";
    "blobs_url" = "https://api.github.com/repos/ReactiveX/RxJava/git/blobs{/sha}";
    "branches_url" = "https://api.github.com/repos/ReactiveX/RxJava/branches{/branch}";
    "clone_url" = "https://github.com/ReactiveX/RxJava.git";
    "collaborators_url" = "https://api.github.com/repos/ReactiveX/RxJava/collaborators{/collaborator}";
    "comments_url" = "https://api.github.com/repos/ReactiveX/RxJava/comments{/number}";
    "commits_url" = "https://api.github.com/repos/ReactiveX/RxJava/commits{/sha}";
    "compare_url" = "https://api.github.com/repos/ReactiveX/RxJava/compare/{base}...{head}";
    "contents_url" = "https://api.github.com/repos/ReactiveX/RxJava/contents/{+path}";
    "contributors_url" = "https://api.github.com/repos/ReactiveX/RxJava/contributors";
    "created_at" = "2013-01-08T20:11:48Z";
    "default_branch" = "2.x";
    "deployments_url" = "https://api.github.com/repos/ReactiveX/RxJava/deployments";
    description = "RxJava \U2013 Reactive Extensions for the JVM \U2013 a library for composing asynchronous and event-based programs using observable sequences for the Java VM.";
    "downloads_url" = "https://api.github.com/repos/ReactiveX/RxJava/downloads";
    "events_url" = "https://api.github.com/repos/ReactiveX/RxJava/events";
    fork = 0;
    forks = 3875;
    "forks_count" = 3875;
    "forks_url" = "https://api.github.com/repos/ReactiveX/RxJava/forks";
    "full_name" = "ReactiveX/RxJava";
    "git_commits_url" = "https://api.github.com/repos/ReactiveX/RxJava/git/commits{/sha}";
    "git_refs_url" = "https://api.github.com/repos/ReactiveX/RxJava/git/refs{/sha}";
    "git_tags_url" = "https://api.github.com/repos/ReactiveX/RxJava/git/tags{/sha}";
    "git_url" = "git://github.com/ReactiveX/RxJava.git";
    "has_downloads" = 1;
    "has_issues" = 1;
    "has_pages" = 1;
    "has_wiki" = 1;
    homepage = "";
    "hooks_url" = "https://api.github.com/repos/ReactiveX/RxJava/hooks";
    "html_url" = "https://github.com/ReactiveX/RxJava";
    id = 7508411;
    "issue_comment_url" = "https://api.github.com/repos/ReactiveX/RxJava/issues/comments{/number}";
    "issue_events_url" = "https://api.github.com/repos/ReactiveX/RxJava/issues/events{/number}";
    "issues_url" = "https://api.github.com/repos/ReactiveX/RxJava/issues{/number}";
    "keys_url" = "https://api.github.com/repos/ReactiveX/RxJava/keys{/key_id}";
    "labels_url" = "https://api.github.com/repos/ReactiveX/RxJava/labels{/name}";
    language = Java;
    "languages_url" = "https://api.github.com/repos/ReactiveX/RxJava/languages";
    "merges_url" = "https://api.github.com/repos/ReactiveX/RxJava/merges";
    "milestones_url" = "https://api.github.com/repos/ReactiveX/RxJava/milestones{/number}";
    "mirror_url" = "<null>";
    name = RxJava;
    "notifications_url" = "https://api.github.com/repos/ReactiveX/RxJava/notifications{?since,all,participating}";
    "open_issues" = 36;
    "open_issues_count" = 36;
    owner =             {
    
    };
    
    
    private = 0;
    "pulls_url" = "https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}";
    "pushed_at" = "2017-03-07T13:17:23Z";
    "releases_url" = "https://api.github.com/repos/ReactiveX/RxJava/releases{/id}";
    score = 1;
    size = 38334;
    "ssh_url" = "git@github.com:ReactiveX/RxJava.git";
    "stargazers_count" = 22101;
    "stargazers_url" = "https://api.github.com/repos/ReactiveX/RxJava/stargazers";
    "statuses_url" = "https://api.github.com/repos/ReactiveX/RxJava/statuses/{sha}";
    "subscribers_url" = "https://api.github.com/repos/ReactiveX/RxJava/subscribers";
    "subscription_url" = "https://api.github.com/repos/ReactiveX/RxJava/subscription";
    "svn_url" = "https://github.com/ReactiveX/RxJava";
    "tags_url" = "https://api.github.com/repos/ReactiveX/RxJava/tags";
    "teams_url" = "https://api.github.com/repos/ReactiveX/RxJava/teams";
    "trees_url" = "https://api.github.com/repos/ReactiveX/RxJava/git/trees{/sha}";
    "updated_at" = "2017-03-08T02:54:40Z";
    url = "https://api.github.com/repos/ReactiveX/RxJava";
    watchers = 22101;
    "watchers_count" = 22101;
    */


}
