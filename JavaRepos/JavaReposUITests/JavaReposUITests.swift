//
//  JavaReposUITests.swift
//  JavaReposUITests
//
//  Created by Ewerton Pereira on 07/03/17.
//  Copyright © 2017 BoomSoft. All rights reserved.
//

import XCTest

class JavaReposUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
    
        continueAfterFailure = false
        XCUIApplication().launch()

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRepoList() {
        
        let app = XCUIApplication()
        let table = app.tables.element
        XCTAssertTrue(table.exists)
        
        let cell = table.cells.elementBoundByIndex(0)
       let existPredicate = NSPredicate(format: "exists == 1", argumentArray: nil)
        self.expectationForPredicate(existPredicate, evaluatedWithObject: cell, handler: nil)
        
        waitForExpectationsWithTimeout(10, handler: nil)
        
        XCTAssertTrue(cell.exists)
        
        XCUIApplication().tables.cells.containingType(.StaticText, identifier:"RxJava").staticTexts["ReactiveX"].tap()
        
        let pullTable = app.tables.element
        XCTAssertTrue(pullTable.exists)
        
        let pullCell = pullTable.cells.elementBoundByIndex(0)
        self.expectationForPredicate(existPredicate, evaluatedWithObject: pullCell, handler: nil)
        
        waitForExpectationsWithTimeout(10, handler: nil)
        
        XCUIApplication().navigationBars["RxJava"].buttons["Github JavaPop"].tap()
        
    }
    
}
